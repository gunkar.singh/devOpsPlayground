import { FC, useState, ChangeEvent } from "react"
import { ITask } from "./interfaces/ITask"
import TodoTask from "./components/ToDoTask"
import "./App.css"

const App: FC = () => {
    const [task, setTask] = useState<string>("")
    const [deadline, setDealine] = useState<number>(0)
    const [todoList, setTodoList] = useState<ITask[]>([])

    const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
        if (event.target.name === "task") {
            setTask(event.target.value)
        } else {
            setDealine(Number(event.target.value))
        }
    }

    const addTask = (): void => {
        if (!task || /^\s*$/.test(task)) {
            setTask("")
            setDealine(0)
            return
        }

        const newTask = { taskName: task, deadline }
        setTodoList([...todoList, newTask])
        setTask("")
        setDealine(0)
    }

    const completeTask = (taskNameToDelete: string): void => {
        setTodoList(todoList.filter(toDo => toDo.taskName !== taskNameToDelete))
    }

    return (
        <div className="App">
            <div className="header">
                <div className="inputContainer">
                    <input
                        type="text"
                        placeholder="Task..."
                        name="task"
                        value={task}
                        onChange={handleChange}
                    />
                    <input
                        type="number"
                        placeholder="Deadline (in Days)..."
                        name="deadline"
                        value={deadline}
                        onChange={handleChange}
                    />
                </div>
                <button type="button" onClick={addTask}>
                    Add Task
                </button>
            </div>
            <div className="todoList">
                {todoList.map((toDo: ITask) => {
                    const randomKey = Math.floor(Math.random() * 10000)

                    return (
                        <TodoTask
                            key={randomKey}
                            task={toDo}
                            completeTask={completeTask}
                        />
                    )
                })}
            </div>
        </div>
    )
}

export default App
