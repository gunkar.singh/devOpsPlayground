import { ITask } from "../interfaces/ITask"

interface ToDoProps {
    task: ITask
    completeTask(task: string): void
}

const TodoTask = ({ task, completeTask }: ToDoProps) => (
    <div className="task">
        <div className="content">
            <span>{task.taskName}</span>
            <span>{task.deadline}</span>
        </div>
        <button
            type="button"
            onClick={() => {
                completeTask(task.taskName)
            }}
        >
            X
        </button>
    </div>
)

export default TodoTask
