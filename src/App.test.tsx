import { render, screen } from '@testing-library/react';
import App from './App';

test('Add task button is shown', () => {
    render(<App />);
    const button = screen.getByText(/Add Task/i);
    expect(button).toBeInTheDocument();
});
